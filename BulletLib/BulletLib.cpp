#include "bullet_BulletLib.h"
#include "BulletWorld.h"
#include "JniBulletConv.h"
#include "BulletMotionStateFactory.h"
#include "BulletJniMotionState.h"

#include <iostream>
#include <algorithm>

using bt::BulletWorld;

template<class T>
bool checkArg( T* arg, const char * log )
{
	if(arg == nullptr)
	{
		std::cerr << log;
		return false;
	}
	return true;
}

// Sync visuals function with used jni ids
static jclass clsTypeICollisionReporter = 0;
static jmethodID collisoinID = 0;
static bool updateSyncVisualsStaticJniIDs( JNIEnv *env )
{
	clsTypeICollisionReporter = env->FindClass( "def/shared/interfaces/ICollisonReporter" );
	collisoinID = env->GetMethodID( clsTypeICollisionReporter, "collision", "(Ldef/shared/interfaces/IPhysicsObjectData;Ldef/shared/interfaces/IPhysicsObjectData;)V" );
	return checkArg(collisoinID, "collision");
}

static jclass clsTypeIRayHitReporter = 0;
static jmethodID reportRayHitID = 0;
bool updateRayHitStaticJniIDs( JNIEnv *env )
{
	clsTypeIRayHitReporter = env->FindClass("def/shared/interfaces/IRayHitReporter");
	checkArg(clsTypeIRayHitReporter, "Could not find class def/shared/interfaces/IRayHitReporter.");

	reportRayHitID = env->GetMethodID( clsTypeIRayHitReporter, "rayHit", "([FLdef/shared/interfaces/IPhysicsObjectData;)[F" );
	return checkArg(reportRayHitID, "Could not find method-id for rayHit!");
}

bool initStaticJNIIDs( JNIEnv *env )
{
	const bool res = updateSyncVisualsStaticJniIDs( env );
	return res && updateRayHitStaticJniIDs( env );
}

JNIEXPORT void JNICALL Java_bullet_BulletLib_initSimulationWithoutGround(JNIEnv *env, jclass)
{
	initStaticJNIIDs( env );

	BulletWorld::resetIntstance();
	BulletWorld::intstance().init();
}


/*
 * Class:     bullet_BulletLib
 * Method:    initSimulation
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_bullet_BulletLib_initSimulation(JNIEnv *env, jclass, jlong groundy)
{
	initStaticJNIIDs( env );

	BulletWorld::resetIntstance();
	BulletWorld::intstance().init();
	BulletWorld::intstance().createGround( groundy, BulletMotionStateFactory::createDefaultMotionState );
}


JNIEXPORT void JNICALL Java_bullet_BulletLib_simulate(JNIEnv *, jclass, jlong)
{
	BulletWorld::intstance().simulate();
}

JNIEXPORT jfloatArray JNICALL Java_bullet_BulletLib_getGravity(JNIEnv* env, jclass)
{
	static const int arraySize = 3;
	jfloatArray result = env->NewFloatArray(arraySize);
	if( NULL == result ){
		return NULL; // out of memory error
	}

	const btVector3& gravity( BulletWorld::intstance().getGravity( ) );
	env->SetFloatArrayRegion(result, 0, arraySize, &gravity.m_floats[0] );

	return result;
}

JNIEXPORT void JNICALL Java_bullet_BulletLib_setGravity(JNIEnv *env, jclass jc, jfloatArray floatArray)
{
	jsize size = env->GetArrayLength( floatArray );
	if( 3 > size)
		return;

	jfloat buffer[3];
	env->GetFloatArrayRegion(floatArray, 0, size, buffer);
	BulletWorld::intstance().setGravity(btVector3(buffer[0], buffer[1], buffer[2]));
}

btVector3 calcImpulse(const btVector3 &from, const btVector3 &to, float force)
{
	btVector3 impulse( to );
	impulse -= from;
	impulse.normalize( );
	impulse *= force;

	return impulse;
}

btVector3 calcDir(const btVector3 &from, const btVector3 &to)
{
	btVector3 dir( to );
	dir -= from;
	dir.normalize( );

	return dir;
}

JNIEXPORT void JNICALL Java_bullet_BulletLib_shootRay(JNIEnv *env, jclass, jfloatArray fromArray, jfloatArray toArray, jobject reporter)
{
	btVector3 from( btjni::btVec3FromJniFloatArray( env, fromArray ) );
	btVector3 to( btjni::btVec3FromJniFloatArray( env, toArray ) );

	btVector3 res;
	btRigidBody* body = BulletWorld::intstance().shootRayNearestResult( from, to, res );
	if(nullptr != body)
	{
		// Hit (first detected)
		const float force = 80;
			
		body->activate( true );

		jfloatArray dir = btjni::btVec3ToLocalJniFloatArray( env, calcDir( from, to ) );
		jfloatArray impulse = (jfloatArray) env->CallObjectMethod( reporter, reportRayHitID, dir, static_cast<jobject>( body->getUserPointer() ) );

		body->applyImpulse( btjni::btVec3FromJniFloatArray( env, impulse ), btVector3() );
	}
}

// AddRigid body funtion with statis jni IDs
static jfieldID typeFileldID = 0;
static jfieldID dimensionsFileldID = 0;
static jfieldID physicsObjDataFileldID = 0;
static void updateStaticAddRigidBodyFieldIDs( JNIEnv* env, jclass clsShapeInfo )
{
	if(!typeFileldID)
		typeFileldID = env->GetFieldID( clsShapeInfo, "type", "Lbullet/adapter/CollisionShapeConstuctionInfo$Type;");
	if(!dimensionsFileldID)
		dimensionsFileldID = env->GetFieldID( clsShapeInfo, "dimensions", "[F");
	if(!physicsObjDataFileldID)
		physicsObjDataFileldID = env->GetFieldID( clsShapeInfo, "physicsObjData", "Ldef/shared/interfaces/IPhysicsObjectData;");
}

std::unique_ptr<btCollisionShape> getCollisionShape( const char* nameChararr, const btVector3& dimVec )
{
	if( strcmp( nameChararr, "CUBE" ) == 0 ) 
	{	
		return std::unique_ptr<btCollisionShape>( new btBoxShape( dimVec ) );
	}
	else
	{
		return std::unique_ptr<btCollisionShape>( new btSphereShape( dimVec[0] ) );
	}
}

JNIEXPORT jlong JNICALL Java_bullet_BulletLib_addRigidBody( JNIEnv* env, jclass, jfloatArray pos, jobject shapeInfo, jfloat mass )
{
	jclass clsShapeInfo = env->GetObjectClass( shapeInfo );

	// ** Get CollisionShapeConstuctionInfo::type, dimensions and physicsObjData field ids **
	updateStaticAddRigidBodyFieldIDs( env, clsShapeInfo );

	if( !typeFileldID || ! dimensionsFileldID || !physicsObjDataFileldID)
	{
		return 0;
	}

	// ** Get member fields usings their ids **
	jobject typeFieldObj = env->GetObjectField( shapeInfo, typeFileldID );
	jfloatArray dimensionsFieldObj = static_cast<jfloatArray>(env->GetObjectField( shapeInfo, dimensionsFileldID ));
	jobject physicsObjDataFieldObj = env->GetObjectField( shapeInfo, physicsObjDataFileldID );

	if( !typeFieldObj || !dimensionsFieldObj || !physicsObjDataFileldID)
	{
		return 0;
	}
	
	// ** Get CollisionShapeConstuctionInfo::physicsObjData postionable **
	// * First, get IPostionable by calling getIPositionable method
	jclass clsTypePhysicsObjData = env->GetObjectClass( physicsObjDataFieldObj );
	jmethodID getIPositionableID = env->GetMethodID( clsTypePhysicsObjData, "getIPostionable", "()Ldef/shared/interfaces/IPositionable;" );
	if( !getIPositionableID )
	{
		return 0;
	}

	// * Call getIPostionable method *
	jobject postionable = env->CallObjectMethod( physicsObjDataFieldObj, getIPositionableID );

	// ** Get getName method ID **
	jclass clsType = env->GetObjectClass( typeFieldObj );
	jmethodID getnameid = env->GetMethodID( clsType, "getName", "()Ljava/lang/String;" );
	if( !getnameid )
	{
		return 0;
	}

	// ** Call getName method to.. get the name **
	const char* nameChararr = "";
	jobject res = env->CallObjectMethod( typeFieldObj, getnameid );
	if( res != NULL )
	{
		nameChararr = env->GetStringUTFChars( ( jstring )res, 0 );
	}

	// Read position and dimensions into btVector3 instances
	btVector3 posVec( btjni::btVec3FromJniFloatArray( env, pos ) );
	btVector3 dimVec( btjni::btVec3FromJniFloatArray( env, dimensionsFieldObj ) );

	//** Create cube rigid body type **
	BulletWorld::BodyRefID idRef = 0;
	
	btTransform startTransform( btQuaternion( 0, 0, 0, 1 ), posVec );
	std::unique_ptr<btMotionState> motionState( BulletMotionStateFactory::createJniMotionState( startTransform, postionable ) );
		
	std::unique_ptr<btCollisionShape> shape( getCollisionShape( nameChararr, dimVec ) );
		
	// Create global ref of user data and add body
	idRef = BulletWorld::intstance().addRigidBody( std::move( motionState ), std::move( shape ) , mass, env->NewGlobalRef( physicsObjDataFieldObj ) );

	long long returnID = idRef;
	return returnID;
}

JNIEXPORT jboolean JNICALL Java_bullet_BulletLib_removeRigidBody( JNIEnv* env, jclass, jlong id )
{
	return BulletWorld::intstance().removeRigidBody( BulletWorld::BodyRefID( id ),
		[=]( const btRigidBody& b )
		{
			jobject globalRef = static_cast<jobject>( b.getUserPointer() );
			env->DeleteGlobalRef( globalRef );
		}
	);
}

/** Synching, not thread-safe, called sequentially
*/ 
JNIEXPORT void JNICALL Java_bullet_BulletLib_syncVisuals( JNIEnv *env, jclass, jobject collisionReporter )
{
	BulletWorld::intstance().syncVisuals(
		[&]( btMotionState& ms )
		{
			BulletJniMotionState& jniMs = static_cast<BulletJniMotionState&>(ms);
			jniMs.syncVisualJavaObject( env );
		}
	);

	if( nullptr == collisionReporter)
	{
		return;
	}

	BulletWorld::intstance().checkForCollisions(
		[&]( const btRigidBody& body0, const btRigidBody& body1 )
		{
			if( !collisoinID )
			{
				return;
			}

			// * Call collision method *
			jobject physDataA = static_cast<jobject>( body0.getUserPointer() );
			jobject physDataB = static_cast<jobject>( body1.getUserPointer() );
			env->CallVoidMethod( collisionReporter, collisoinID, physDataA, physDataB );
		}
	);
}
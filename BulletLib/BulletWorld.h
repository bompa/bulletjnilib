#ifndef _BULLETWORLD_H_
#define _BULLETWORLD_H_

#include <btBulletDynamicsCommon.h>
#include <LinearMath\btMotionState.h>
#include <memory>
#include <stdint.h>
#include <unordered_map>
#include <functional>

namespace bt
{
	/** Add a rigid body to the world */
	void addToWorld(btDynamicsWorld& world, btRigidBody& body );
	
	/** Remove a rigid body from the world */
	void removeFromWorld(btDynamicsWorld& world, btRigidBody& body );

	typedef std::function<std::unique_ptr<btMotionState>( const btTransform& )> CreateDefaultMotionStateFunc;
	typedef std::function<void(const btRigidBody& body0, const btRigidBody& body1)> CollisionCallbackFuncT;
	typedef std::function<void(const btRigidBody& body0, const btRigidBody& body1)> RayHitCallbackFuncT;

	template <class BodyT>
	struct btBodyCtorInfoTraits{
		typedef btRigidBody::btRigidBodyConstructionInfo ctorInfoT;
	};

	// TODO: Specialize for other types

	/**
	 * Implementation wrapper around the bullet physics simulation world
	 */
	class BulletWorld
	{
	public:
		// internal types
		typedef std::unique_ptr<BulletWorld> UniquePtr;
		typedef BulletWorld& Ref;
		typedef BulletWorld* Ptr;
		typedef long long BodyRefID;

		/** Moveable, noncopyable body reference added to world */
		template<class BodyT, class UserPtrT = void*>
		class WorldBodyRef
		{
			WorldBodyRef(btDynamicsWorld& world, const typename btBodyCtorInfoTraits<BodyT>::ctorInfoT& ctorInfo, void* userPtr = nullptr )
				: mWorld( world ), mBody( new btRigidBody( ctorInfo ) )
			{
				mBody->setUserPointer( userPtr );
				bt::addToWorld( mWorld, *mBody );
			}

		public:
			static WorldBodyRef<BodyT> addBody( btDynamicsWorld& world, const typename btBodyCtorInfoTraits<BodyT>::ctorInfoT& ctorInfo, void* userPtr = NULL )
			{
				return WorldBodyRef<BodyT>( world, ctorInfo, userPtr );
			}

			static std::unique_ptr< WorldBodyRef<BodyT> > addBodyGetPtr( btDynamicsWorld& world, const typename btBodyCtorInfoTraits<BodyT>::ctorInfoT& ctorInfo, void* userPtr = NULL )
			{
				return std::unique_ptr< WorldBodyRef<BodyT> >( new WorldBodyRef<BodyT>( world, ctorInfo, userPtr ) );
			}

			WorldBodyRef( WorldBodyRef&& rhs )
				: mWorld( rhs.mWorld ), mBody( std::move( rhs.mBody ) )
			{ }

			~WorldBodyRef()
			{
				bt::removeFromWorld( mWorld, *mBody );

				delete mBody->getMotionState( );
				mBody->setMotionState( NULL );

				delete mBody->getCollisionShape( );
				mBody->setCollisionShape( NULL );
			}

			BodyRefID getBodyID( ) const{
				return BodyRefID( mBody.get( ) );
			}

			const BodyT& getBody( ) const {
				return *mBody;
			}

			BodyT& getBody( ) {
				return *mBody;
			}

		private:
			WorldBodyRef( const WorldBodyRef& ){ }
			WorldBodyRef& operator=( const WorldBodyRef& ){ }

		private:
			std::unique_ptr<BodyT> mBody;
			btDynamicsWorld& mWorld;
		};
		typedef std::unordered_map< BodyRefID, std::unique_ptr< WorldBodyRef<btRigidBody> > > BodyRefsMap;

		/** SyncOp called when syncronizting motionstates, abstract class -- removed -- */
		struct SyncOp : public std::unary_function<btMotionState&, void>{
			virtual void operator ()( btMotionState& ms );
		};

	public:
		~BulletWorld(void);

		void init();
		void simulate();
		void syncVisuals( std::function<void( btMotionState& )> );
		void checkForCollisions( CollisionCallbackFuncT ) const;
		btVector3 getGravity( ) const;
		void setGravity( const btVector3 &gravity );
		BodyRefID addRigidBody( std::unique_ptr<btMotionState> motionState, std::unique_ptr<btCollisionShape> shape, float mass, void* userPtr = NULL );
		bool removeRigidBody( BodyRefID id, std::function<void(const btRigidBody& body)> userCleanup = nullptr );
		btRigidBody* shootRayNearestResult( const btVector3& from, const btVector3& to, btVector3& res );
		void createGround( float yHeight, CreateDefaultMotionStateFunc motionStateFactoryFunc );
		const BodyRefsMap& getWorldBodiesMap() const
		{ return mBodyRefs; }

		static void resetIntstance( );
		static Ref intstance( );

	private:
		BulletWorld();
		BulletWorld( const BulletWorld& );
		BulletWorld& operator=( const BulletWorld& );

	private:
		// Bullet configuration pointers
		std::unique_ptr<btBroadphaseInterface>				mBroadphase;
		std::unique_ptr<btCollisionDispatcher>				mDispatcher;
		std::unique_ptr<btConstraintSolver>					mSolver;
		std::unique_ptr<btDefaultCollisionConfiguration>	mCollisionConfiguration;
		std::unique_ptr<btAxisSweep3>						mOverlappingPairCache;

		std::unique_ptr<btDynamicsWorld>					mWorld;

		BodyRefID mGroundBodyID;
		BodyRefsMap mBodyRefs;

		btScalar mTimeStep;
		int mMaxSubSteps; 

		static UniquePtr mInstance;	
		
		static const BodyRefID InvalidBodyRefID;
	};
}

#endif
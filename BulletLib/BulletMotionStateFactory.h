#ifndef _BULLETMOTIONSTATEFACTORY_H_
#define _BULLETMOTIONSTATEFACTORY_H_

#include <btBulletCollisionCommon.h>
#include <memory>
#include <jni.h>

class BulletMotionStateFactory
{
public:
	static std::unique_ptr<btMotionState> createJniMotionState( const btTransform& initalPos, jobject userObect );
	static std::unique_ptr<btMotionState> createDefaultMotionState( const btTransform& initalPos );
};

#endif
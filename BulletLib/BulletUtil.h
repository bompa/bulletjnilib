#ifndef _BULLET_UTIL_H_
#define _BULLET_UTIL_H_

#include <btBulletCollisionCommon.h>
#include <sstream>

namespace bt
{
	template<size_t N>
	void fill3x3OpenGLFloatArray( const btMatrix3x3& rotation, float (&rotMtxArr)[N] )
	{
		//static_assert(N == 9);

		//Col. 1
		rotMtxArr[0] = rotation[0].getX();
		rotMtxArr[1] = rotation[1].getX();
		rotMtxArr[2] = rotation[2].getX();

		//Col. 2
		rotMtxArr[3] = rotation[0].getY();
		rotMtxArr[4] = rotation[1].getY();
		rotMtxArr[5] = rotation[2].getY();

		//Col. 3
		rotMtxArr[6] = rotation[0].getZ();
		rotMtxArr[7] = rotation[1].getZ();
		rotMtxArr[8] = rotation[2].getZ();
	}

	template<size_t N>
	void fill3x3FloatArray( const btMatrix3x3& rotation, float (&rotMtxArr)[N] )
	{
		//static_assert(N == 9);

		//Col. 1
		rotMtxArr[0] = rotation[0].getX();
		rotMtxArr[1] = rotation[0].getY();
		rotMtxArr[2] = rotation[0].getZ();

		//Col. 2
		rotMtxArr[3] = rotation[1].getX();
		rotMtxArr[4] = rotation[1].getY();
		rotMtxArr[5] = rotation[1].getZ();

		//Col. 3
		rotMtxArr[6] = rotation[2].getX();
		rotMtxArr[7] = rotation[2].getY();
		rotMtxArr[8] = rotation[2].getZ();
	}


	std::string print( const btMatrix3x3& m );

	std::string print( const btVector3& v );
}

#endif
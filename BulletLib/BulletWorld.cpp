#include "BulletWorld.h"
#include "BulletUtil.h"
#include <iostream>
#include <LinearMath/btTransform.h>

static btScalar DefaultTimeStep = 0.05f;
static int DefaultMaxSubSteps = 10; 

namespace bt
{
	void addToWorld(btDynamicsWorld& world, btRigidBody& body )
	{
		world.addRigidBody( &body );
	}

	void removeFromWorld(btDynamicsWorld& world, btRigidBody& body )
	{
		world.removeRigidBody( &body );
	}

	BulletWorld::BulletWorld(void)
		: mGroundBodyID( InvalidBodyRefID ), mTimeStep( DefaultTimeStep ), mMaxSubSteps( DefaultMaxSubSteps )
	{ }

	BulletWorld::~BulletWorld(void)
	{
		if( mGroundBodyID != InvalidBodyRefID )
		{
			removeRigidBody( mGroundBodyID );
		}
	}

	// Static instances
	BulletWorld::UniquePtr BulletWorld::mInstance;
	const BulletWorld::BodyRefID BulletWorld::InvalidBodyRefID = -1;

	void BulletWorld::simulate( )
	{
		mWorld->stepSimulation( mTimeStep, mMaxSubSteps );
	}

	void BulletWorld::syncVisuals( std::function<void(btMotionState&)> syncOp )
	{
		BodyRefsMap::const_iterator bodies = mBodyRefs.begin();
		for( ; bodies != mBodyRefs.end( ); bodies++ )
		{
			btRigidBody& body = bodies->second->getBody( );
			if( body.getMotionState() && !body.isStaticObject() )
			{
				syncOp( *body.getMotionState() );
			}
		}
	}

	void BulletWorld::checkForCollisions( CollisionCallbackFuncT handleCollision ) const
	{
		const int numManifolds = mWorld->getDispatcher()->getNumManifolds();
		for( int i = 0; i < numManifolds; i++ )
		{
			btPersistentManifold* contactManifold =  mWorld->getDispatcher()->getManifoldByIndexInternal( i );
			
			if( contactManifold->getNumContacts() > 0 )
			{
				const btRigidBody* obA = (const btRigidBody*) contactManifold->getBody0();
				const btRigidBody* obB = (const btRigidBody*) contactManifold-> getBody1();

				handleCollision( *obA, *obB );
			}
		}
	}

	btVector3 BulletWorld::getGravity( ) const
	{
		return mWorld->getGravity();
	}

	void BulletWorld::setGravity( const btVector3 &gravity )
	{
		mWorld->setGravity( gravity );
	}

	void BulletWorld::init(void)
	{ 
		mCollisionConfiguration.reset( new btDefaultCollisionConfiguration( ) );
		mDispatcher.reset( new btCollisionDispatcher( mCollisionConfiguration.get( ) ) );
	
		mSolver.reset( new btSequentialImpulseConstraintSolver( ) );

		btVector3 worldAabbMin( -10000, -10000, -10000 );
		btVector3 worldAabbMax( 10000, 10000, 10000 );
		int maxProxies = 1024;
		mOverlappingPairCache.reset( new btAxisSweep3( worldAabbMin, worldAabbMax, maxProxies ) );
	
		mWorld.reset( new btDiscreteDynamicsWorld( mDispatcher.get( ), mOverlappingPairCache.get( ), mSolver.get( ), mCollisionConfiguration.get( ) ) );
		mWorld->setGravity( btVector3(0, -10, 0) );
	}

	void BulletWorld::createGround( float yHeight, bt::CreateDefaultMotionStateFunc motionStateFactoryFunc )
	{
		btTransform groundPos;
		groundPos.setIdentity();
		groundPos.setOrigin( btVector3( 0, yHeight, 0 ) );

		const float mass = 0;
		btVector3 localInertia(0, 0, 0);

		std::unique_ptr<btBoxShape> groundShape( new btBoxShape( btVector3( 150.f, 50.f, 150.f ) ) );
		std::unique_ptr<btMotionState> motionState( motionStateFactoryFunc( groundPos ) );
		if( motionState.get() )
		{
			mGroundBodyID = addRigidBody( std::move( motionState ), std::move( groundShape ), mass );
		}
	}

	void BulletWorld::resetIntstance(  )
	{
		mInstance.reset( nullptr );
	}

	BulletWorld::Ref BulletWorld::intstance( )
	{
		if( mInstance == nullptr )
			mInstance.reset( new BulletWorld );

		return *mInstance;
	}

	BulletWorld::BodyRefID BulletWorld::addRigidBody(	std::unique_ptr<btMotionState> motionState,
														std::unique_ptr<btCollisionShape> shape,
														float mass,
														void* userPtr )
	{
		btVector3 localInertia( 0, 0, 0 );

		bool isDynamic = ( mass != 0.0f );
		if (isDynamic) {
			shape->calculateLocalInertia( mass, localInertia );
		}
	
		btRigidBody::btRigidBodyConstructionInfo ctorInfo( mass, motionState.release(), shape.release(), localInertia );

		std::unique_ptr< WorldBodyRef<btRigidBody> > bodyRef( std::move( WorldBodyRef<btRigidBody>::addBodyGetPtr( *mWorld, ctorInfo, userPtr ) ) );
		BodyRefID id = bodyRef->getBodyID( );

		mBodyRefs.insert( std::make_pair( id, std::move( bodyRef ) ) );

		return id;
	}

	bool BulletWorld::removeRigidBody( BulletWorld::BodyRefID id, std::function<void(const btRigidBody& body)> userCleanup )
	{
		BodyRefsMap::iterator res = mBodyRefs.find( id );
		if( res != mBodyRefs.end( ) )
		{
			if( userCleanup )
			{
				userCleanup( res->second->getBody() );
			}

			BodyRefsMap::iterator eraseRes = mBodyRefs.erase( res );
			return true;
		}

		return false;
	}

	btRigidBody* BulletWorld::shootRayNearestResult( const btVector3& from, const btVector3& to, btVector3& res )
	{
		btCollisionWorld::ClosestRayResultCallback result( from, to );
		mWorld->rayTest( from, to, result );

		if( result.hasHit( ) )
		{
			const btVector3& hit = result.m_hitPointWorld;
			res = hit;

			btRigidBody* body = (btRigidBody*) result.m_collisionObject;
			return body;
		}
		return nullptr;
	}

}
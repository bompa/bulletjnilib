#ifndef _BULLETJNIMOTIONSTATE_H_
#define _BULLETJNIMOTIONSTATE_H_

#include <jni.h>
#include <btBulletCollisionCommon.h>

#include <iostream>

#include "BulletUtil.h"

class BulletJniMotionState : public btMotionState {
public:
    BulletJniMotionState( const btTransform &initialpos, jobject userObect )
		: mDefaultMotionState( initialpos ), 
		mVisibleobj( userObect ), 
		mClsType( NULL ), 
		mSetPositionID( NULL ),
		mSetRotationID( NULL ),
		mUpdated( false)
	{ }

	BulletJniMotionState( const btTransform &initialpos )
		: mDefaultMotionState( initialpos ), 
		mVisibleobj( NULL ), 
		mClsType( NULL ), 
		mSetPositionID( NULL ),
		mSetRotationID( NULL ),
		mUpdated( false)
	{ }

    void setJavaObject( jobject visibleObj ) {
        mVisibleobj = visibleObj;
    }

    void getWorldTransform( btTransform &worldTrans ) const {
		return mDefaultMotionState.getWorldTransform( worldTrans );
    }

	/**
	* Called from setSimulation on updated objects
	*/
    void setWorldTransform( const btTransform &worldTrans ) {
        if(NULL == mVisibleobj)
            return; // silently return before we set a node

		mDefaultMotionState.setWorldTransform( worldTrans );
		mUpdated = true;
    }

	void syncVisualJavaObject( JNIEnv* env )
	{
		if(!mUpdated)
		{
			return;
		}
		mUpdated = false;

		static const int rotMtxSz = 9;
		static const int threeDemVecSz = 3;

		// Extract values from transform to position, rotate float arrays
		btTransform worldTrans;
		mDefaultMotionState.getWorldTransform( worldTrans );

		const btMatrix3x3& rotation = worldTrans.getBasis();
		float rotMtxArr[rotMtxSz] = {0};
		bt::fill3x3FloatArray( rotation, rotMtxArr );

		const btVector3& pos = worldTrans.getOrigin();
		float posVecArr[threeDemVecSz] = { pos.getX(), pos.getY(), pos.getZ() };

		// Jni - allocate arrays and get method ids
		if( !checkJniPointers( ) )
		{
			mClsType = env->GetObjectClass( mVisibleobj );
			mSetPositionID = env->GetMethodID( mClsType, "setPosition", "([F)V" );
			mSetRotationID = env->GetMethodID( mClsType, "setRotation", "([F)V" );
		}
		
		if( !checkJniPointers( ) )
		{
			return;
		}

		jfloatArray jposArr = env->NewFloatArray( threeDemVecSz );
		jfloatArray jrotArr = env->NewFloatArray( rotMtxSz );

		if( !jposArr ||! jrotArr ){
			return;
		}

		// Jni - Fill java float arrays
		env->SetFloatArrayRegion( jposArr, 0, threeDemVecSz, posVecArr );
		env->SetFloatArrayRegion( jrotArr, 0, rotMtxSz, rotMtxArr );

		// Jni - Call set object state methods
		env->CallVoidMethod( mVisibleobj, mSetPositionID, jposArr );
		env->CallVoidMethod( mVisibleobj, mSetRotationID, jrotArr );
	}

private:
	bool checkJniPointers()
	{
		return mClsType && mSetPositionID && mSetRotationID && mVisibleobj;
	}

private:
	// Java object jni info
	jobject mVisibleobj;
	jclass mClsType;
	jmethodID mSetPositionID;
	jmethodID mSetRotationID;

	bool mUpdated;

	btDefaultMotionState mDefaultMotionState;
};

#endif
#ifndef _JNIBULLETCONV_H_
#define _JNIBULLETCONV_H_

#include <btBulletDynamicsCommon.h>
#include <jni.h>

namespace btjni
{

	/*template<int N>
	btVector3 btVec3FromFloatArray(const float& (vec)[N])
	{
		return btVector3(vec[0], vec[1], vec[2]);
	}*/
	
	static const int ThreeDemVecSz = 3;

	btVector3 btVec3FromJniFloatArray(JNIEnv* env, jfloatArray floatArray)
	{
		jsize size = env->GetArrayLength( floatArray );
		if( ThreeDemVecSz > size)
			return btVector3();

		float buffer[ThreeDemVecSz];
		env->GetFloatArrayRegion(floatArray, 0, size, buffer);
		return btVector3(buffer[0], buffer[1], buffer[2]);
	}

	jfloatArray btVec3ToLocalJniFloatArray(JNIEnv* env, const btVector3 vec3 )
	{
		jfloatArray floatArray = env->NewFloatArray( ThreeDemVecSz );
		if( !floatArray ){
			return nullptr;
		}

		float vecArr[ThreeDemVecSz] = { vec3.getX(), vec3.getY(), vec3.getZ() };
		env->SetFloatArrayRegion( floatArray, 0, ThreeDemVecSz, vecArr );
		
		return floatArray;
	}
}

#endif
#include "BulletUtil.h"

namespace bt
{
	std::string print( const btMatrix3x3& m )
	{
		std::stringstream ss;

		ss << m[0].getX() << " " << m[1].getX() << " " << m[2].getX() << std::endl
			<< "\t\t" << m[0].getY() << " " << m[1].getY() << " " << m[2].getY() << std::endl
			<< "\t\t" << m[0].getZ() << " " << m[1].getZ() << " " << m[2].getZ() << std::endl;

		return ss.str();
	}

	std::string print( const btVector3& v )
	{
		std::stringstream ss;
		ss << v[0] << " " << v[1] << " " << v[2] << std::endl;

		return ss.str();
	}
}
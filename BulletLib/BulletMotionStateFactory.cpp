#include "BulletMotionStateFactory.h"
#include "BulletJniMotionState.h"

std::unique_ptr<btMotionState> BulletMotionStateFactory::createJniMotionState( const btTransform& initalPos, jobject userObect )
{
	std::unique_ptr<btMotionState> ptr( new BulletJniMotionState( initalPos, userObect ) );
	return std::move( ptr );
}

std::unique_ptr<btMotionState> BulletMotionStateFactory::createDefaultMotionState( const btTransform& initalPos )
{
	std::unique_ptr<btMotionState> ptr( new BulletJniMotionState( initalPos ) );
	return std::move( ptr );
}
